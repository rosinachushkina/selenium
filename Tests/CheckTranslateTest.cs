﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using Selenium.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using static Pages.Pages.PhotosPage;
using static Pages.Pages.PostsPage;
using static Pages.Pages.TodosPage;

namespace RestSharpExamples.Tests
{
    [TestFixture]
    public class BasicTests
    {
        [Test]
        public void StatusCodeTest()
        {

            RestClient restClient = new RestClient(PagesUrl.MainPage);
            var request = new RestRequest("posts", Method.GET);

            IRestResponse response = restClient.Execute(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }



        [Test]
        public void CreatePostRequest()
        {
            RestClient restClient = new RestClient(PagesUrl.MainPage);


            JObject jObjectbody = new JObject();
            jObjectbody.Add("userId", "1");
            jObjectbody.Add("id", "150");
            jObjectbody.Add("title", "dolore veritatis porro provident adipisci blanditiis et sun");
            jObjectbody.Add("body", "test");

            RestRequest restRequest = new RestRequest("/posts", Method.POST);

            restRequest.AddParameter("application/json", jObjectbody, ParameterType.RequestBody);

            IRestResponse restResponse = restClient.Execute(restRequest);
            int StatusCode = (int)restResponse.StatusCode;
            Assert.AreEqual(201, StatusCode, "Status code is not 201");
            //http://jsonplaceholder.typicode.com/posts
            //    Check that new post can be added into the system

        }


        [Test]
        public void UpdatePostRequest()
        {
            RestClient restClient = new RestClient(PagesUrl.MainPage);



            UpdateJson updateJson = new UpdateJson()
            {
                userId = 1,
                id = 1,
                title = "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                body = "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
            };
         


        string jsonText = JObject.FromObject(updateJson).ToString();

            RestRequest restRequest = new RestRequest("/posts/1", Method.PUT);

            restRequest.AddParameter("application/json", jsonText, ParameterType.RequestBody);
            IRestResponse restResponse = restClient.Execute(restRequest);

            string response = restResponse.Content;
            RestClient responceObject = JsonConvert.DeserializeObject<RestClient>(response);
            string responceText = JObject.FromObject(responceObject).ToString();

            Assert.AreEqual(response, responceText.Replace("\r", ""), "The response attributes aren't equal the actual result");

            //http://jsonplaceholder.typicode.com/posts
            //    Check that new post can be updated into the system
        }
      

        [Test]
        public void DeletePostRequest()
        {


            RestClient restClient = new RestClient(PagesUrl.MainPage);
            RestRequest restRequest = new RestRequest("posts/1", Method.DELETE);
            IRestResponse restResponse = restClient.Execute(restRequest);
            int StatusCode = (int)restResponse.StatusCode;
            Assert.AreEqual(200, StatusCode, "Status code is not 200");
            //http://jsonplaceholder.typicode.com/posts
            //    Check that new post can be deleted from the system
        }
        [Test]
        public void WebService()
        {
            RestClient restClient = new RestClient(PagesUrl.MainPage);

            RestRequest restRequest = new RestRequest("/posts", Method.GET);

            IRestResponse restResponse = restClient.Execute(restRequest);

            string response = restResponse.Content;
            JArray jsonResponse = (JArray)JsonConvert.DeserializeObject(response);
            List<PostObject> messageList = jsonResponse.ToObject<List<PostObject>>();

            //List<Message> messageList = JsonConvert.DeserializeObject<List<Message>>(jsonResponse);

            foreach (PostObject message in messageList)
            {
                if (message.body.Contains("ipsum dolorem"))
                {
                    Assert.AreEqual("marcia@name.biz", message.email);
                }
            }

        }
        [Test]
        public void UserNameCheck()
        {
            RestClient restClient = new RestClient(PagesUrl.MainPage);

            RestRequest restRequest = new RestRequest("/posts", Method.GET);

            IRestResponse restResponse = restClient.Execute(restRequest);

            string response = restResponse.Content;
            JArray jsonResponse = (JArray)JsonConvert.DeserializeObject(response);
            List<PostObject> messageList = jsonResponse.ToObject<List<PostObject>>();

            foreach (PostObject message in messageList)
            {
                if (message.body.Contains("eos dolorem iste accusantium est eaque quam"))
                {
                    Assert.AreEqual("Patricia Lebsack", message.name);
                }
            }
            //https://jsonplaceholder.typicode.com/posts
            //            Check if user who posted a post with title "eos dolorem iste accusantium est eaque quam"
            //name is "Patricia Lebsack"

        }
        [Test]
        public void PhotoTitleCheck()
        {
            RestClient restClient = new RestClient(PagesUrl.MainPage);

            RestRequest restRequest = new RestRequest("/photos", Method.GET);

            IRestResponse restResponse = restClient.Execute(restRequest);

            string response = restResponse.Content;
            JArray jsonResponse = (JArray)JsonConvert.DeserializeObject(response);
            List<PhotosClass> bodyList = jsonResponse.ToObject<List<PhotosClass>>();


            foreach (PhotosClass bod in bodyList)
            {
                if (bod.title.Contains("ad et natus qui"))
                {
                    Assert.AreEqual(21, bod.id);
                }
            }
            //https://jsonplaceholder.typicode.com/photos
            //            Check if photo with title "ad et natus qui" belongs to user with email "Sincere@april.biz"

        }
        [Test]
        public void TodosCheck()
        {
            RestClient restClient = new RestClient(PagesUrl.MainPage);

            RestRequest restRequest = new RestRequest("/todos", Method.GET);

            IRestResponse restResponse = restClient.Execute(restRequest);

            string response = restResponse.Content;
            JArray jsonResponse = (JArray)JsonConvert.DeserializeObject(response);
            List<TodosClass> todosList = jsonResponse.ToObject<List<TodosClass>>();


            foreach (TodosClass todos in todosList)
            {
                 if (todos.completed.Equals("true"))
                {
                    Assert.AreEqual(12, todos.id);
                }
            }
            //https://jsonplaceholder.typicode.com/todos 
            //            check that  Leanne Graham has more than 3 "completed" TODOs than Ervin Howell

        }


        //https://jsonplaceholder.typicode.com/photos <<Need to implement binary comparison, should pass
        //    //            check if image of photo with id 4 isn't corrupted.Use file d32776.png for referrence (see attachment).
        //}

    }

    }
    